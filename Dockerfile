FROM debian
MAINTAINER Jhi-Yong Bhang <jcornil@gmail.com>

# Install debconf-utils & wget
RUN \
	apt-get update && \
	apt-get install -y \
		debconf-utils \
		wget \
		curl \
		git

# Add dotdeb repository
ADD dotdeb.list /etc/apt/sources.list.d/
RUN \
	wget https://www.dotdeb.org/dotdeb.gpg && \
	apt-key add dotdeb.gpg && \
	apt-get update

# Install MariaDB
RUN \
	echo mysql-server mysql-server/root_password password root | debconf-set-selections; \
  	echo mysql-server mysql-server/root_password_again password root | debconf-set-selections; \
  	apt-get install -y \
  		mariadb-server \
		mariadb-client

# Install PHP7 & Nginx
RUN	\
	apt-get install -y \
		php7.0 \
		php7.0-fpm \
		php7.0-bz2 \
		php7.0-curl \
		php7.0-dba \
		php7.0-gd \
		php7.0-json \
		php7.0-mbstring \
		php7.0-mcrypt \
		php7.0-memcached \
		php7.0-mysql \
		php7.0-opcache \
		php7.0-redis \
		php7.0-sqlite3 \
		php7.0-xml \
		php7.0-xmlrpc \
		php7.0-zip \
		nginx-full && \
		rm -rf /etc/php/7.0/fpm/pool.d/www.conf

ADD www.conf /etc/php/7.0/fpm/pool.d/

# Install Composer 
RUN \
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	php -r "unlink('composer-setup.php');" && \
	mv composer.phar /usr/local/bin/composer

